#!/usr/bin/env python

import ctypes
import time

import vlc

instance = vlc.Instance('--clock-synchro=0', '--network-caching=0')


opt = vlc.str_to_bytes('sout-all')
options = [opt]
print(options)

instance.vlm_add_broadcast(
    'stream/2',
    'rtsp://gosc:gosc123@83.238.87.201/ISAPI/Streaming/channels/2',
    '#rtp{sdp=rtsp://:8554/stream/2,proto=tcp}',
    0,
    None, 
    True,
    0
)

instance.vlm_add_broadcast(
    'stream1',
    'rtsp://gosc:gosc123@83.238.87.201/ISAPI/Streaming/channels/2',
    '#transcode{vcodec=theo,vb=1024,channels=1,ab=128,samplerate=44100,width=320}:http{dst=:8080/webcam1.ogg}',
    3,
    (
        b':rtsp-tcp',
        b':clock-synchro=0',
        b':network-caching=0'
    ),
    True,
    0
)
instance.vlm_add_broadcast(
    'stream2',
    'rtsp://gosc:gosc123@83.238.87.202/ISAPI/Streaming/channels/2',
    '#transcode{vcodec=theo,vb=1024,channels=1,ab=128,samplerate=44100,width=320}:http{dst=:8080/webcam2.ogg}',
    3,
    [
        'rtsp-tcp'.encode('utf-8'),
        'clock-synchro=0'.encode('utf-8'),
        'network-caching=0'.encode('utf-8'),
        0
    ],
    True,
    0
)
#instance.vlm_play_media('stream1')
instance.vlm_play_media('stream/2')
print(instance.vlm_show_media('stream/2'))
try:
    while True:
        time.sleep(1)
        #print(instance.vlm_show_media('stream/2'))
except KeyboardInterrupt:
    pass
#instance.vlm_stop_media('stream1')
instance.vlm_stop_media('stream/2')
